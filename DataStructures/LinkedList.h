#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "Node.h"
#include <iostream>

using namespace std;

class LinkedList
{
private:
	Node* head;
	int size;
public:
	LinkedList();
	bool insertNode(int d);
	bool deleteNode(int index);
	Node* find(int data);
	void printList();
	~LinkedList()
	{
		Node* curN = head;
		Node* nextN;
		while (curN != nullptr)
		{
			nextN = curN->next;
			delete curN;
			curN = nextN;
		}
	}
};

#endif