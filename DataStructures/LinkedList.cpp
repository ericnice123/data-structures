#include "LinkedList.h"

LinkedList::LinkedList()
{
	head = nullptr;
	size = 0;
}

bool LinkedList::insertNode(int d)
{
	if (head == nullptr)
	{
		Node* h = new Node(d);
		head = h;
		size++;
		return true;
	}
	
	Node* cur = head;
	while (cur->next != nullptr)
	{
		cur = cur->next;
	}
	Node* newN = new Node(d);
	cur->next = newN;
	size++;
	return true;
}

bool LinkedList::deleteNode(int d)
{
	Node* cur = head;
	if (cur->data == d)
	{
		head = cur->next;
		size--;
		delete cur;
		cur = NULL;
		return true;
	}

	while (cur)
	{
		if (cur->next->data == d)
		{
			Node* deleteMe = cur->next;
			cur->next = cur->next->next;
			delete deleteMe;
			deleteMe = NULL;
			size--;
			return true;
		}
		cur = cur->next;
	}
	return false;

}

void LinkedList::printList()
{
	Node* cur = head;
	for (int i = 0; i < size; i++)
	{
		cout << cur->data << endl;
		cur = cur->next;
	}
}